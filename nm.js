const Nightmare = require("nightmare");
const nightmare = Nightmare({ show: false });
var argv = require("optimist").argv;

const url = "https://pesnityt.net/s.php";
const query = "?q=" + argv.query;

nightmare
  .goto(`${url}${query}`)
  .evaluate(() => {
    var res = [];
    $(".blok_pesni").each(function(item) {
      res.push({
        nametrack: $(this)
          .find(".isint")
          .children("a")
          .eq(0)
          .text(),
        artist: $(this)
          .find(".isint")
          .children("a")
          .eq(1)
          .text(),
        hrefSong: $(this)
          .find(".play > span")
          .attr("data-url")
      });
    });
    return res;
  })
  .end()
  .then(res => {
    res.map(item => {
      console.log(item.nametrack + ":|:" + item.artist + ":|:" + item.hrefSong);
    });
  })
  .catch(error => {
    console.error("Search failed:", error);
  });
