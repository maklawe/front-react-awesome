const request = require("request");
const querystring = require("querystring");
const queryString = require("query-string");

const oauth2 = require("simple-oauth2").create({
  client: {
    id: "866a97d84fa44e0487ea6b6c0ff752e2",
    secret: "458d904643d34ee0baeb603c835b2eb9"
  },
  auth: {
    tokenHost: "https://accounts.spotify.com",
    authorizePath: "/authorize",
    tokenPath: "/api/token"
  }
});

let accessToken;

oauth2.clientCredentials.getToken({}, (error, result) => {
  if (error) {
    return console.log("Access Token Error", error.message);
  }
  accessToken = oauth2.accessToken.create(result);
});

module.exports.send = function(data, callback) {
  request(
    {
      url: `http://api.spotify.com/v1/${data.method}?${queryString.stringify(
        data.data
      )}`,
      auth: { bearer: accessToken.token.access_token }
    },
    function(err, response, body) {
      console.log(err, body);
      if (err) {
        return callback(error.message);
      }
      callback(body);
    }
  );
};

module.exports.song = function(data, callback) {
  console.log(`http://api.spotify.com/v1/tracks/${data.id}`, data);
  request(
    {
      url: `https://open.spotify.com/embed?uri=spotify:track:${data.id}`,
      auth: { bearer: accessToken.token.access_token }
    },
    function(err, response, body) {
      console.log(err, body);
      if (err) {
        return callback(error.message);
      }
      callback(body);
    }
  );
};

module.exports.gettrack = function(data, callback) {
  request(
    {
      url: `https://api.spotify.com/v1/me/player/currently-playing?market=ES`,
      auth: { bearer: accessToken.token.access_token }
    },
    function(err, response, body) {
      console.log(err, body);
      if (err) {
        return callback(error.message);
      }
      callback(body);
    }
  );
};
