const express = require("express");
const fs = require("fs");
const bodyParser = require("body-parser");
const spotiSender = require("./lib/spoty-sender");
const exec = require("child_process").exec;
const app = express();

let router = express.Router();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../build"));

app.get("/", function(req, res) {
  console.log(__dirname);
  res.sendFile("/../build/index.html", { root: __dirname });
});

app.post("/updateList", function(req, res) {
  fs.writeFileSync("save.log", JSON.stringify(req.body), { flag: "w" });
  res.send();
});

app.get("/loadStorage", function(req, res) {
  var save = fs.readFileSync("save.log");
  res.send(save);
});

function execNM(query, callback) {
  exec(`node ./nm --query=${query}`, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    var res = { tracks: [] };
    stdout.split("\n").map(item => {
      var parts = item.split(":|:");
      res.tracks.push({
        artist: parts[0],
        trackName: parts[1],
        href: parts[2]
      });
    });
    callback(res);
  });
}

app.post("/send", function(req, res) {
  var body = req.body;
  execNM(req.body.query, data => {
    res.send(data);
  });
});

app.post("/save", function(req, res) {
  fs.writeFileSync("save.log", JSON.stringify(req.body), { flag: "w" });
  res.send();
});

app.listen(3000, function() {
  console.log("Example app listening on port 3000!");
});
