import React, { Component } from "react";
import { Button, Icon, Form } from "semantic-ui-react";

export default class Audio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      duration: null,
      sliderValue: 0,
      currentTimeInterval: null
    };
  }

  handlePlay() {
    this.audio.play();
  }

  handleStop() {
    this.audio.pause();
  }

  componentDidMount() {
    // Get duration of the song and set it as max slider value
    this.audio.onloadedmetadata = function() {
      this.handlePlay();
      this.setState({ duration: this.audio.duration });
    }.bind(this);
    // Sync slider position with song current time
    this.audio.onplay = () => {
      var interval = setInterval(() => {
        this.setState({
          sliderValue: this.audio.currentTime
        });
      }, 500);

      this.setState({
        currentTimeInterval: interval
      });
    };

    this.audio.onpause = () => {
      clearInterval(this.state.currentTimeInterval);
    };

    this.setState({
      currentTimeInterval: null
    });
  }
  sliderChange = e => {
    clearInterval(this.state.currentTimeInterval);
    this.audio.currentTime = e.target.value;
  };

  render() {
    const src = `https://pesnityt.net${this.props.storage.player.songHref}`;
    return (
      <div>
        <audio
          ref={audio => {
            this.audio = audio;
          }}
          src={src}
        />
        <Button.Group>
          <Button icon>
            <Icon name="left arrow" />
          </Button>
          <Button icon onClick={this.handlePlay.bind(this)}>
            <Icon name="play" />
            Play
          </Button>

          <Button icon onClick={this.handleStop.bind(this)}>
            <Icon name="pause" />
            Pause
          </Button>
          <Button icon>
            <Icon name="right arrow" />
          </Button>
        </Button.Group>
        <Form.Input
          min={0}
          max={this.state.duration}
          step={1}
          type="range"
          onChange={this.sliderChange}
          value={this.state.sliderValue}
        />
      </div>
    );
  }
}
