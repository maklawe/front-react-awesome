import React, { Component } from "react";
import { List, Icon, Input } from "semantic-ui-react";
import ChangeName from "./ChangeName";

export default class ShowLists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ListActive: true
    };
  }

  render() {
    var playList = this.props.storage.list;
    var template = playList.map((item, index) => {
      return (
        <ChangeName
          storage={this.props.storage}
          item={item}
          key={"playerList" + index}
          number={index}
        />
      );
    });
    return <List>{template}</List>;
  }
}
