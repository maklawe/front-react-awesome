import React, { Component } from "react";

import CreateList from "./CreateList";
import ShowLists from "./ShowLists";

export default class Menu extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <CreateList storage={this.props.storage} />
        <ShowLists storage={this.props.storage} />
      </div>
    );
  }
}
