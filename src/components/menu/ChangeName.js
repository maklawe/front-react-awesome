import React, { Component } from "react";
import { Input, List, Icon } from "semantic-ui-react";

export default class ChangeName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ListActive: true,
      name: this.props.item.name
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      name: nextProps.item.name
    });
  }

  rename = () => {
    this.setState({
      ListActive: false
    });
  };

  handleChange = e => {
    this.setState({ name: e.target.value });
  };

  saveName = () => {
    this.setState({
      ListActive: true
    });
  };

  render() {
    var current = (
      <div>
        <List.Item
          className={"list-item"}
          onClick={() => this.props.storage.chose(this.props.number)}
          as="a"
        >
          {this.state.name}
        </List.Item>
        <Icon
          size={"big"}
          className="icon-change"
          onClick={this.rename}
          name={"write"}
        />
        <Icon
          className="icon-delete"
          size="big"
          name={"close"}
          onClick={() => this.props.storage.deleteList(this.props.number)}
        />
      </div>
    );
    if (this.state.ListActive) {
      return current;
    }
    current = (
      <div>
        <Input
          className="inputlist"
          value={this.state.name}
          onChange={this.handleChange}
        />
        <Icon
          className="icon-list"
          name={"check"}
          size={"big"}
          onClick={this.saveName}
        />
      </div>
    );
    return current;
  }
}
