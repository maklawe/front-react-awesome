import React, { Component } from "react";
import { Icon, Input, Button } from "semantic-ui-react";

export default class CreateList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      nameList: ""
    };
  }
  openInput = () => {
    this.setState({
      active: true
    });
  };

  createNewList = () => {
    var nameList = this.state.nameList;
    this.setState({
      nameList: "",
      active: false
    });
    if (nameList.length == 0) {
      return;
    }
    this.props.storage.pushNewList(nameList);
  };

  clearInputList = () => {
    this.setState({
      nameList: "",
      active: false
    });
  };

  handleChange = e => {
    this.setState({ nameList: e.target.value });
  };
  render() {
    if (this.state.active) {
      return (
        <div>
          <Input
            className="inputlist"
            value={this.state.nameList}
            onChange={this.handleChange}
          />
          <Icon
            className="icon-list"
            name={"plus"}
            size={"big"}
            onClick={this.createNewList}
          />
          <Icon
            className="icon-list"
            name={"minus"}
            size={"big"}
            onClick={this.clearInputList}
          />
        </div>
      );
    }

    return (
      <Button className="buttoncreate" onClick={this.openInput}>
        Create playlist
      </Button>
    );
  }
}
