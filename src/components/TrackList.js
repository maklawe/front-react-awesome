import React, { Component } from "react";

export default class TrackList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    var choose = this.props.storage.choose;
    if (choose !== null) {
      var chooseList = this.props.storage.list[choose];
      return (
        <div>
          <p>{chooseList.name}</p>
          {chooseList.songs.map(song => {
            return (
              <div>
                {song.artist} - {song.trackName}
              </div>
            );
          })}
        </div>
      );
    }
    return null;
  }
}
