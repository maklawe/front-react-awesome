import React, { Component } from "react";
import { Input, List, Icon, Dropdown, Button } from "semantic-ui-react";

export default class Song extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    var list = this.props.storage.list;
    return (
      <List.Item>
        <Button icon className="playsong">
          <Icon
            name="play"
            onClick={() => {
              this.props.storage.setPlayerSongHref(this.props.about.href);
            }}
          />
        </Button>
        <p>
          {this.props.about.artist} - {this.props.about.trackName}
        </p>
        <Dropdown
          button
          pointing
          className="link item dropbutton"
          text="Добавить в"
        >
          <Dropdown.Menu>
            {list.map((item, index) => {
              return (
                <Dropdown.Item
                  onClick={() => {
                    this.props.storage.pushToList(index, this.props.about);
                  }}
                >
                  {item.name}
                </Dropdown.Item>
              );
            })}
          </Dropdown.Menu>
        </Dropdown>
      </List.Item>
    );
  }
}
