import React, { Component } from "react";
import { Input, List, Icon } from "semantic-ui-react";
import Song from "./Song";

export default class SearchPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    var current = this.props.storage.searchPanel.currentSearch;

    var names;
    if (current.tracks) {
      names = current.tracks.map(item => {
        return <Song about={item} storage={this.props.storage} />;
      });
    }

    var inputValue = this.props.storage.searchPanel.inputValue;
    return (
      <div>
        <Input
          loading={this.props.storage.searchPanel.loading}
          icon="search"
          placeholder="Search"
          className="inputlist"
          value={inputValue}
          onChange={this.props.storage.searchPanel.setInputName}
        />
        <Icon
          className="iconsearch"
          name="check"
          size="big"
          onClick={this.props.storage.searchPanel.searchOn}
        />
        <List>{names}</List>
      </div>
    );
  }
}
