import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Grid, Button, Icon } from "semantic-ui-react";
import ReactAplayer from "react-aplayer";

import Menu from "./components/menu";
import TrackList from "./components/TrackList";
import SearchPanel from "./components/SearchPanel";
import Audio from "./components/audio";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      storage: {
        choose: null,
        list: [],
        setPlayerSongHref: this.setPlayerSongHref,
        pushToList: this.pushToList,
        chose: this.chose,
        deleteList: this.deleteList,
        showTrack: this.showTrack,
        pushNewList: this.pushNewList,
        player: {
          songHref: ""
        },
        searchPanel: {
          inputValue: "",
          currentSearch: {},
          loading: false,

          setInputName: this.setInputName,
          searchOn: this.searchOn
        }
      }
    };
    this.load();
  }

  update = newStorage => {
    this.setState({
      storage: newStorage
    });
    fetch("/save", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.state.storage)
    });
  };

  load = () => {
    fetch("/loadStorage", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        var newStorage = this.state.storage;
        newStorage.choose = data.choose;
        newStorage.list = data.list;
        newStorage.player = data.player;
        this.update(newStorage);
      })
      .catch(err => {
        console.log("error", err);
      });
  };

  searchOn = () => {
    var newStorage = this.state.storage;
    newStorage.searchPanel.loading = true;
    this.setState({
      storage: newStorage
    });
    const requestData = {
      query: this.state.storage.searchPanel.inputValue
    };
    fetch("/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(requestData)
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        var newStorage = this.state.storage;
        newStorage.searchPanel.currentSearch = data;
        newStorage.searchPanel.loading = false;

        this.update(newStorage);
      })
      .catch(err => {
        console.log("error", err);
      });
  };
  setPlayerSongHref = href => {
    var newStorage = this.state.storage;
    newStorage.player.songHref = href;

    this.setState({
      storage: newStorage
    });
  };

  pushToList = (index, about) => {
    var newStorage = this.state.storage;
    newStorage.list[index].songs.push(about);

    this.update(newStorage);
  };
  chose = index => {
    var newStorage = this.state.storage;
    newStorage.choose = index;

    this.setState({
      storage: newStorage
    });
  };

  deleteList = (index, e) => {
    var newStorage = this.state.storage;
    newStorage.list.splice(index, 1);

    this.update(newStorage);
  };

  setInputName = e => {
    var newStorage = this.state.storage;
    newStorage.searchPanel.inputValue = e.target.value;

    this.setState({
      storage: newStorage
    });
  };

  showTrack = trackName => {
    var newStorage = this.state.storage;
    newStorage.playListName = trackName;

    this.setState({
      storage: newStorage
    });
  };

  pushNewList = nameList => {
    var newStorage = this.state.storage;
    newStorage.list.push({ name: nameList, songs: [] });

    this.update(newStorage);
  };

  render() {
    return (
      <Grid className="main">
        <Grid.Column width={4} className="tracklist">
          <Menu storage={this.state.storage} />
        </Grid.Column>
        <Grid.Column width={8}>
          <TrackList storage={this.state.storage} />
          <div className="footer">
            <Audio storage={this.state.storage} />
          </div>
        </Grid.Column>
        <Grid.Column width={4} className="searchpanel">
          <SearchPanel storage={this.state.storage} />
        </Grid.Column>
      </Grid>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
